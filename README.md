Taffer's Tilesets
=================
![This is a big world map. I like how the maps look.][1]

Polished tilesets and palettes for the ASCII faithful, featuring a stylish letter 'd' for dwarves. Optional keybindings are included: they allow diagonal movement without a number pad

You'll need to use a [better text editor][2] to edit these files in Windows.

Diagonal Walls
==============
![This is a preview of vile heresy!][3]

The diagonal walls tend to be fancier looking than my straight walls, and unlike most diagonal walls, square rooms still look pretty good with these tilesets. Blocks of solid wall look much nicer as well: this is best seen in Arena mode.

Three fonts are available with diagonal walls: sans-serif, cyrillic-inspired, and arabic-inspired.

Straight Walls
==============
![This is a preview of boring orthodoxy.][4]

If you want straight walls, these are for you. For the more traditional and orthagonally minded among you. Only sans-serif is available with straight walls.

![!Lots of color schemes are available!][5]

[1]: https://gitlab.com/dwarf-fortress/taffer/raw/master/previews/world-map.gif
[2]: https://notepad-plus-plus.org/
[3]: https://gitlab.com/dwarf-fortress/taffer/raw/master/previews/fonts.gif
[4]: https://gitlab.com/dwarf-fortress/taffer/raw/master/previews/traditional.png
[5]: https://gitlab.com/dwarf-fortress/taffer/raw/master/previews/palettes.gif
